import pandas as pd
import numpy as np
from datetime import timedelta

path = 'H:\\Python scripts\\Machine Learning\\Transient\\Dataset\\'

data = pd.read_csv(path + 'transient_raw_data_v2.csv', encoding='cp437')
data['RoomTypeID'] = data['Property'] + data['Booked Room Category Label']

# replacing country name
country_list = {'Congo': ['Congo, The Democratic Republic', 'Congo, the Democratic Republic'],
                 'Korea (North)': ["Korea, Democratic People's Rep", "Korea,Democratic People's Repu"],
                 'Korea (South)': ['Korea, Republic Of'],
                 'Great Britain': ['United Kingdom', 'Virgin Islands, British', 'Saint Helena', 'South Georgia and the South Sa'],
                 'Iran': ['Iran, Islamic Republic of'],
                 'Laos': ["Lao People's Democratic Republ"],
                 'Netherlands': ['Netherlands Antilles', 'Sint Maarten'],
                 'France': ['French Guiana', 'French Polynesia'],
                 'Taiwan': ['Republic of China, Taiwan'],
                 'Russia': ['Russian Federation'],
                 'Syria': ['Syrian Arab Republic'],
                 'Tanzania': ['Tanzania, United Republic of'],
                 'United States': ['United States Minor Outlying I', 'American Samoa', 'Midway Islands'],
                 'Vietnam': ['Viet Nam'],
                 'Burkina Faso': ['Burkina Fasa'],
                 'Kazakhstan': ['Kazakstan'],
                 'Kyrgyzstan': ['Kyrgzstan'],
                 'Micronesia, Federal States of' : ['Micronesia, Federated States o'],
                 'nan' : ['Unknown', 'XX - (UNKNOWN)']}
country_list = {v: k for k, vs in country_list.items() for v in vs}
data['Country Name'].replace(country_list, inplace=True)

# add region
region_path = 'H:\\Python scripts\\Machine Learning\\Transient\\preprocessing\\'

region_list = pd.read_excel(region_path + 'country_region_list.xlsx')
region_list = region_list.set_index('COUNTRY')['REGION'].to_dict()
data['Region'] = data['Country Name'].replace(region_list)

# Room type
room_type = pd.read_excel(region_path + 'RoomType.xlsx')
room_type['RoomTypeID'] = room_type['Property'] + room_type['RoomType']
room_type = room_type[['RoomTypeID', 'Property', 'Category', 'RoomCategory', 'RoomType']]
room_type = room_type[~room_type['Property'].isin(['FourSeasons', 'GrandSuiteFS', 'PaizaMansions', 'Sheraton', 'StRegis'])]
room_type.drop_duplicates(subset=['RoomTypeID'], keep='last', inplace=True)
room_type = room_type[['RoomTypeID', 'RoomType', 'Category', 'RoomCategory']]


data = pd.merge(data, room_type, how='left', left_on='RoomTypeID', right_on='RoomTypeID')
data.drop(['RoomTypeID', 'Booked Room Category Label'], axis=1, inplace=True)

data.to_csv(path + 'transient_raw_data_v3.csv', index=False)
