import pandas as pd
import numpy as np
from datetime import timedelta

path = 'H:\\Python scripts\\Machine Learning\\Transient\\Dataset\\'

data = pd.read_csv(path + 'transient_raw_data_v1.csv', encoding='cp437')

# Convert to datetime format
data['Arrival Date'] = pd.to_datetime(data['Arrival Date'])
data['Arrival Month'] = data['Arrival Date'].dt.month
data['Arrival Day'] = data['Arrival Date'].dt.day
data['Departure Date'] = pd.to_datetime(data['Departure Date'])
data['Departure Month'] = data['Departure Date'].dt.month
data['Departure Day'] = data['Departure Date'].dt.day

# combine insert date and time
data['Insert Date'] = pd.to_datetime(data['Insert Date'])
data['Insert Month'] = data['Insert Date'].dt.month
data['Insert Day'] = data['Insert Date'].dt.day

data['Insert Time'] = pd.to_datetime(data['Insert Time'], errors='coerce').dt.time

# calculate no. of weekday day and weekend day
data['Departure_out'] = data[['Arrival Date','Departure Date']].apply(lambda x: x[1] if x[0] == x[1] else x[1] - timedelta(days=1), axis=1)
data['weekday_day'] = data[['Arrival Date','Departure_out']].apply(lambda x: len(pd.bdate_range(start=x[0],end=x[1], freq='C', weekmask='1111001')), axis=1)
data['weekend_day'] = data[['Arrival Date','Departure_out']].apply(lambda x: len(pd.bdate_range(start=x[0],end=x[1], freq='C', weekmask='0000110')), axis=1)


# calculate holiday category
data['holiday'] = ''

holiday_path = 'H:\\Python scripts\\Machine Learning\\Transient\\preprocessing\\'

holiday_data = pd.read_excel(holiday_path + 'HolidayTable.xlsx', sheets='2018-19 23')

holiday_data = holiday_data.groupby(['Holiday', 'Year']).agg({'Date': [np.min, np.max]})
holiday_data.columns = holiday_data.columns.droplevel(0)
holiday_data.reset_index(inplace=True)
holiday_data['Year'] = holiday_data['Year'].apply(str)
holiday_data['Holiday'] = holiday_data['Holiday'] + ' ' + holiday_data['Year']
holiday_data.drop(['Year'], axis=1, inplace=True)
holiday_data.columns = ['Holiday', 'Start', 'End']
holiday_data['Start'] = holiday_data['Start'].dt.strftime('%Y-%m-%d')
holiday_data['End'] = holiday_data['End'].dt.strftime('%Y-%m-%d')
holiday = holiday_data.set_index('Holiday').T.to_dict('list')

for name, date in holiday.items():
    mask = (((data['Arrival Date'] <= date[0]) & (data['Departure Date'] >= date[1])) | ((data['Arrival Date'] >= date[0]) & (data['Arrival Date'] <= date[1])) | ((data['Departure Date'] >= date[0]) & (data['Departure Date'] <= date[1])))
    data.loc[mask, 'holiday'] = name


# calculate LOS and lead days
data['LOS'] = (data['Departure Date'] - data['Arrival Date']).dt.days
data['lead day'] = (data['Arrival Date'] - data['Insert Date']).dt.days

# get insert hour and minute
time_range = [i for i in range(0, 25, 2)]
time_labels = [str(i) + '-' + str(i+2) for i in range(0, 24, 2)]

data['Insert Time'] = data['Insert Time'].apply(str)
data['Insert Hour'] = data['Insert Time'].str.split(':').str[0]
data['Insert Hour'] = data['Insert Hour'].apply(int)
data['Insert Hour'] = pd.cut(data['Insert Hour'], time_range, labels=time_labels)
data['Insert Hour'] = data['Insert Hour'].astype(str)

data.drop(['Departure_out'], axis=1, inplace=True)

data.to_csv(path + 'transient_raw_data_v2.csv', index=False)
