import pandas as pd
import numpy as np
from datetime import timedelta

path = 'H:\\Python scripts\\Machine Learning\\Transient\\Dataset\\'

data = pd.read_csv(path + 'Room Cancellation Raw for prediction.csv', encoding='cp437')

drop_columns_v1 = ['Business Date', 'Guest Last Name', 'Guest First Name', 'Reservation Status', 'Business Block (Code)',
                   'Company Name', 'E-mail', 'Phone No', 'ROW_ID', 'ConfirmNo', 'Room Type Label']
data.drop(drop_columns_v1, axis=1, inplace=True)

data.dropna(subset=['Extraction_Date'], inplace=True, axis=0)

# filter by segment
data = data[data['Segment'].isin([20, 30])]
# filter by Cash/Comp
data = data[~data['Cash/Comp'].isin(['COMP'])]
# filter by Channel
data = data[~data['Channel'].isin(['INTERNAL'])]
# filter by Market(Code)
data = data[~data['Market (Code)'].isin(['CAMA1', 'CAMA2', 'CAMA3', 'CAMA4', 'CAMA5', 'ADMIN'])]
# filter by property
data = data[~data['Property'].isin(['HolidayInn'])]
# filter by year
data['Arrival Date'] = pd.to_datetime(data['Arrival Date'])
data['Arrival Year'] = data['Arrival Date'].dt.year
data = data[data['Arrival Year'].isin([2018, 2019, 2023])]


# change column value to boolean
data['Room/Package'] = data['Room/Package'].apply(lambda x: 1 if x == 'R' else (2 if x == 'P' else np.nan))

# Definite and Cancel label
data['Status'] = data['TrueBooking'].apply(lambda x: 1 if x == 1 else 0)
mask = (data['Status_Gianthotel'] == 'NO SHOW')
data.loc[mask, 'Status'] = 0

drop_columns_v2 = ['Segment', 'Extraction_Date', 'TrueBooking', 'Status_Gianthotel', 'Cash/Comp']
data.drop(drop_columns_v2, axis=1, inplace=True)

data.to_csv(path + 'transient_raw_data_v1.csv')
