import pandas as pd
import numpy as np
from datetime import timedelta

path = 'H:\\Python scripts\\Machine Learning\\Transient\Dataset\\'
data = pd.read_csv(path + 'transient_raw_data_v3.csv', encoding='cp437')

rate_code_path = 'H:\\Python scripts\\Machine Learning\\Transient\\preprocessing\\'
rate_code = pd.read_excel(rate_code_path + 'rate_code_tables.xlsx')
rate_code_column_drop = ['Rate Description', 'Cash_Product']
rate_code.drop(rate_code_column_drop, axis=1, inplace=True)

# merge rate code by begin and end booking date in rate code table
data = data.reset_index()
rate_code = rate_code.reset_index()

data['Insert Date'] = pd.to_datetime(data['Insert Date'])
rate_code['Begin Booking Date'] = pd.to_datetime(rate_code['Begin Booking Date'])
rate_code['End Booking Date'] = pd.to_datetime(rate_code['End Booking Date'])

data.dropna(subset=['Rate Code'], inplace=True)

df_h = data.merge(rate_code, on = ['Rate Code'], how = 'inner', suffixes = ['_df1', '_df2'])
df_h = df_h[(df_h['Insert Date'] >= df_h['Begin Booking Date']) & (df_h['Insert Date'] <= df_h['End Booking Date'])]
df_h = df_h[['index_df1','index_df2']].set_index('index_df1')

rate_code = rate_code.rename(columns = {'index': 'match_index'})

df_h = data.join(df_h).rename(columns = {'index_df2': 'match_index'})
df = df_h.merge(rate_code, on = ['Rate Code', 'match_index']).drop(['match_index'], axis = 1)
df.drop_duplicates(subset=['index'], keep='last', inplace=True)

# join back the cannot merge records
ratecode_cannot_join = df['index'].tolist()
df2 = data[~data.index.isin(ratecode_cannot_join)]

data = pd.concat([df, df2])
data.drop(['Unnamed: 0'], axis=1, inplace=True)

data.to_csv(path + 'transient_raw_data_v4.csv', index=False)
