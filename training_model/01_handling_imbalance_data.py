import numpy as np
import pandas as pd
from datetime import datetime

import category_encoders as ce
from sklearn.preprocessing import StandardScaler
from sklearn.compose import ColumnTransformer

from imblearn.over_sampling import SMOTENC
from imblearn.under_sampling import TomekLinks


file = 'H:\\Python scripts\\Machine Learning\\Transient\\Dataset\\transient_raw_data_v4.csv'
data = pd.read_csv(file, encoding='cp437')

data = data.drop(['Begin Booking Date', 'End Booking Date', 'Travel Agent Name', 'ACCOUNT', 'Origin (Code)', 'State (Full Name)', 
                  'Rate Code', 'Confirmation No', 'index', 'Insert Time', 'Departure Date', 'Arrival Date', 'Insert Date', 'Arrival Year'], axis=1)
data.rename({'Proj. Room Revenue Ex. Tax over': 'Room_Rate', 'Proj. Room Nights over the Repor': 'Number_of_Room'}, axis=1, inplace=True)

# take out the year in holiday name
data['holiday'] = data['holiday'].str[:-5]


#replace_category_np = []
#data[replace_category_np] = data[replace_category_np].replace('-', np.NaN)
#TODO: Check how to convert NaN value for oversampling
data.fillna(0, inplace=True)

column_name = ['Room/Package', 'Arrival Month', 'Arrival Day', 'Departure Month', 'Departure Day','Insert Month', 'Insert Day', 
               'weekday_day', 'weekend_day', 'LOS', 'lead day', 'Room_Rate', 'Number_of_Room', 'breakfast', 'discount', 
               'advance_purchase', 'LOS_package', 'MAR_rate', 'package_TA']
data[column_name] = data[column_name].astype(int)

columns_to_te = ['Property', 'Channel', 'Market (Code)', 'Source (Code)', 'Country Name', 'holiday', 'Insert Hour', 'Region', 
                 'RoomType', 'Category', 'RoomCategory', 'Cash_ProductType']
data[columns_to_te] = data[columns_to_te].astype(str)


X = data.drop(['Status'], axis=1)
y = data['Status'].astype(int)


# # Oversampling
start_time_1 = datetime.now()

# smote = SMOTENC(categorical_features=['Property', 'Channel', 'Market (Code)', 'Source (Code)', 'Country Name', 'holiday', 'Insert Hour', 'Region', 
#                                       'RoomType', 'Category', 'RoomCategory', 'Cash_ProductType'], random_state=0)
# X_train_over, y_train_over = smote.fit_resample(X, y)

# end_time_1 = datetime.now()
# print(X_train_over.shape, y_train_over.shape)
# print('Duration: {}'.format(end_time_1 - start_time_1))


# transformer data before undersampling
transformers = [('standarize', StandardScaler(), column_name),
                ('te', ce.TargetEncoder(), columns_to_te)]
ct = ColumnTransformer(transformers, remainder='passthrough')

ct.fit(X, y)
X_under = ct.transform(X)

# Undersampling
start_time_2 = datetime.now()

tl = TomekLinks()
X_train_under, y_train_under = tl.fit_resample(X_under, y)

end_time_2 = datetime.now()
print(X_train_under.shape, y_train_under.shape)
print('Duration: {}'.format(end_time_2 - start_time_2))
